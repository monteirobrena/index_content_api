# INDEX CONTENT API

This project is an RESTful API builded with Ruby on Rails to get and store the content of web page.

## Endpoints

### /pages [GET]

Get all pages stored.

### /pages [POST]

Store a new content of web page

#### Params
{ "page": { "link": "https://www.nytimes.com" } }

## Tests

### Rspec

Run `rspec spec/`

### Test coverage

Run `open coverage/index.html`