module API::V1
  class PagesController < ApplicationController
    ATTRIBUTES = [:link]

    def index
      @pages = Page.all

      render json: @pages, status: :ok
    end

    def create
      begin
        page = Page.create
        link = Link.create(url: page_params[:link], page: page)
        Tag.add_tags(link, page)

        render json: page, status: :created
      rescue => e
        render json: { page: { errors: e.message } }, status: :unprocessable_entity
      end
    end

    def page_params
      return {} if params[:page].blank?

      params.require(:page).permit(ATTRIBUTES)
    end
  end
end
