class Link < ActiveRecord::Base
  URL_REGEXP = /\A(http|https):\/\/|[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,6}(:[0-9]{1,5})?(\/.*)?\z/ix

  validates :url, presence: true
  validates :url, format: { with: URL_REGEXP, allow_nil: false }

  belongs_to :page
end
