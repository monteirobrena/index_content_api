class Page < ActiveRecord::Base
  has_one  :link
  has_many :tags, dependent: :destroy
end
