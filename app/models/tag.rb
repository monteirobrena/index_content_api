class Tag < ActiveRecord::Base
  TAGS = %w(h1 h2 h3)

  validates :name, :content, presence: true
  validates :name, inclusion: { in: TAGS }

  belongs_to :page

  def self.add_tags(link, page)
    agent = Mechanize.new
    @content_page = agent.get(link.url)

    raise "Body is empty" if @content_page.body.blank?

    @tags = []
    @page = page

    TAGS.each { |tag_name| create_tags(tag_name) }
  end

  def self.create_tags(tag_name)
    @content_page.search(tag_name).each { |tag| @tags << Tag.create(name: tag_name, content: tag.text, page: @page) }
  end
end
