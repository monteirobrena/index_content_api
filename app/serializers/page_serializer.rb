class PageSerializer < ActiveModel::Serializer
  attributes :id

  has_one :link
  has_many :tags
end
