class TagSerializer < ActiveModel::Serializer
  attributes :id, :name, :content
end
