Rails.application.routes.draw do
  namespace :api, defaults: { format: :json }, path: '/' do
    scope module: :v1 do
      resources :pages, only: [:index, :create]
    end
  end
end
