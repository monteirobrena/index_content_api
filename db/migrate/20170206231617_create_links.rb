class CreateLinks < ActiveRecord::Migration
  def change
    create_table :links do |t|
      t.string :url
      t.belongs_to :page, index: true

      t.timestamps null: false
    end

    add_index :links, :id, unique: true
  end
end
