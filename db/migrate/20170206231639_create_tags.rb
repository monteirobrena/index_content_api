class CreateTags < ActiveRecord::Migration
  def change
    create_table :tags do |t|
      t.string :name
      t.string :content
      t.belongs_to :page, index: true

      t.timestamps null: false
    end

    add_index :tags, :id, unique: true
  end
end
