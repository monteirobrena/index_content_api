class CreatePages < ActiveRecord::Migration
  def change
    create_table :pages do |t|

      t.timestamps null: false
    end

    add_index :pages, :id, unique: true
  end
end
