class CreatePagesLinks < ActiveRecord::Migration
  def change
    create_table :pages_links do |t|
      t.belongs_to :page, index: true
      t.belongs_to :link, index: true
    end
  end
end
