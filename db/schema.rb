# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170206233850) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "links", force: :cascade do |t|
    t.string   "url"
    t.integer  "page_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "links", ["id"], name: "index_links_on_id", unique: true, using: :btree
  add_index "links", ["page_id"], name: "index_links_on_page_id", using: :btree

  create_table "pages", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "pages", ["id"], name: "index_pages_on_id", unique: true, using: :btree

  create_table "pages_links", force: :cascade do |t|
    t.integer "page_id"
    t.integer "link_id"
  end

  add_index "pages_links", ["link_id"], name: "index_pages_links_on_link_id", using: :btree
  add_index "pages_links", ["page_id"], name: "index_pages_links_on_page_id", using: :btree

  create_table "pages_tags", force: :cascade do |t|
    t.integer "page_id"
    t.integer "tag_id"
  end

  add_index "pages_tags", ["page_id"], name: "index_pages_tags_on_page_id", using: :btree
  add_index "pages_tags", ["tag_id"], name: "index_pages_tags_on_tag_id", using: :btree

  create_table "tags", force: :cascade do |t|
    t.string   "name"
    t.string   "content"
    t.integer  "page_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "tags", ["id"], name: "index_tags_on_id", unique: true, using: :btree
  add_index "tags", ["page_id"], name: "index_tags_on_page_id", using: :btree

end
