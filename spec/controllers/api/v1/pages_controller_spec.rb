require 'rails_helper'

describe API::V1::PagesController do
  context "GET #index" do
    context "when errors are not found" do
      before do
        create_list(:page, 3)
        get :index, format: :json
      end

      let(:pages_response) { JSON.parse(response.body, symbolize_names: true) }

      it { should respond_with :ok }
      it { expect(pages_response.size).to eql(Page.all.count) }
    end

    context "when pages are empty" do
      before do
        get :index, format: :json
      end

      let(:pages_response) { JSON.parse(response.body, symbolize_names: true) }

      it { should respond_with :ok }
      it { expect(pages_response).to eql([]) }
    end
  end

  context "POST #create" do
    context "when errors are not found" do
      before do
        stub_request(:get, "https://nytimes.com")

        post :create, { page: { link: "https://www.nytimes.com" } }, format: :json
      end

      let(:pages_response) { JSON.parse(response.body, symbolize_names: true) }

      it { should respond_with :created }
      it { expect(pages_response[:link][:url]).to eql("https://www.nytimes.com") }
      it { expect(pages_response[:tags]).to_not be_empty }
    end

    context "when errors are found" do
      context "when tags wasn't found" do
        before do
          stub_request(:get, "http://localhost:3000").with(body: "")

          post :create, { page: { link: "http://localhost:3000" } }, format: :json
        end

        let(:pages_response) { JSON.parse(response.body, symbolize_names: true) }

        it { should respond_with :unprocessable_entity }
        it { expect(pages_response).to_not be_empty }
        it { expect(pages_response.has_key?(:page)).to be_truthy }
        it { expect(pages_response[:page].has_key?(:errors)).to be_truthy }
        it { expect(pages_response[:page][:errors]).to eql("Body is empty") }
      end

      context "when URL is invalid" do
        before do
          post :create, { page: { link: "123" } }, format: :json
        end

        let(:pages_response) { JSON.parse(response.body, symbolize_names: true) }

        it { should respond_with :unprocessable_entity }
        it { expect(pages_response).to_not be_empty }
        it { expect(pages_response.has_key?(:page)).to be_truthy }
        it { expect(pages_response[:page].has_key?(:errors)).to be_truthy }
        it { expect(pages_response[:page][:errors]).to eql("absolute URL needed (not 123)") }
      end
    end
  end
end
