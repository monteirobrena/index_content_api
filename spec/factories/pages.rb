FactoryGirl.define do
  factory :page do
    association :link, strategy: :build

    before(:create) { |page| page.tags << build(:tag) }
  end
end
