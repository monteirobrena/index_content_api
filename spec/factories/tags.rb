FactoryGirl.define do
  factory :tag do
    content { FFaker::Lorem.phrase }
    name { Tag::TAGS.sample }
  end
end
