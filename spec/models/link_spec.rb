require 'rails_helper'

RSpec.describe Link, type: :model do
  context 'validations' do
    it { should validate_presence_of(:url) }
    it { should allow_value("https://vimeo.com/178841667", "vimeo.com/178841667").for(:url) }
    it { should_not allow_value("12345 my website", "contact@email").for(:url) }
  end

  context 'relationships' do
    it { should belong_to(:page) }
  end
end
