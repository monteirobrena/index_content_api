require 'rails_helper'

RSpec.describe Page, type: :model do
  context 'relationships' do
    it { should have_one(:link) }
    it { should have_many(:tags).dependent(:destroy) }
  end
end
