require 'rails_helper'

RSpec.describe Tag, type: :model do
  context 'validations' do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:content) }
    it { should validate_inclusion_of(:name).in_array(Tag::TAGS) }
  end

  context 'relationships' do
    it { should belong_to(:page) }
  end

  describe '.add_tags' do
    let(:page) { create(:page) }
    let(:link) { create(:link, url: "https://www.nytimes.com") }

    it { expect(Tag.add_tags(link, page)).to_not be_empty }
  end
end
